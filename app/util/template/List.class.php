<?php

ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

class **LIST_CLASS_NAME** extends TStandardList
{
    protected $form;     // registration form
    protected $datagrid; // listing
    protected $pageNavigation;
    protected $formgrid;
    protected $deleteButton;
    protected $transformCallback;

    /**
     * Page constructor
     */
    public function __construct()
    {

        parent::__construct();

        parent::setDatabase('**DB_CONFIG_FILE**');            // defines the database
        parent::setActiveRecord('**RECORD_NAME**');   // defines the active record
        parent::setDefaultOrder('id', 'asc');         // defines the default order

**SEARCH_ITEMS**

        // creates the form
        $this->form = new BootstrapFormBuilder('list_**TABLE_NAME**'); 
        $this->form->setFormTitle( _t('**LIST_LABEL**') );

**SEARCH_FIELDS**

        // keep the form filled during navigation with session data
        $this->form->setData( TSession::getValue($this->activeRecord.'_filter_data') );

        // add actions buttons with events to form
        $btn = $this->form->addAction(_t('Find'), new TAction(array($this, 'onSearch')), 'fa:search');
        $btn->class = 'btn btn-sm btn-primary';
        $this->form->addAction(_t('New'),  new TAction(array('**FORM_NAME**', 'onEdit')), 'bs:plus-sign green');
        $this->form->addAction( 'Limpar Busca' , new TAction(array($this, 'onClear')), 'fa:eraser red');

        //DATAGRID ------------------------------------------------------------------------------------------
        //$this->datagrid = new TDatagridTables();
        $this->datagrid = new BootstrapDatagridWrapper(new TDataGrid);
        $this->datagrid->datatable = 'true';
        $this->datagrid->style = 'width: 100%';
        $this->datagrid->setHeight(320);

**DATA_GRID_ITEMS_LINE**

        // add actions buttons with events to grid
        $action_edit = new TDataGridAction(array('**FORM_NAME**', 'onEdit'));
        $action_edit->setButtonClass('btn btn-default');
        $action_edit->setLabel(_t('Edit'));
        $action_edit->setImage('fa:pencil-square-o blue fa-lg');
        $action_edit->setField('id');
        $this->datagrid->addAction($action_edit);

        $action_del = new TDataGridAction(array($this, 'onDelete'));
        $action_del->setButtonClass('btn btn-default');
        $action_del->setLabel(_t('Delete'));
        $action_del->setImage('fa:trash-o red fa-lg');
        $action_del->setField('id');
        $this->datagrid->addAction($action_del);

        $this->datagrid->createModel();

        // create the page navigation
        $this->pageNavigation = new TPageNavigation;
        $this->pageNavigation->setAction(new TAction(array($this, 'onReload')));
        $this->pageNavigation->setWidth($this->datagrid->getWidth());

        $panel = new TPanelGroup;
        $panel->add($this->datagrid);
        $panel->addFooter($this->pageNavigation);

        // vertical box container
        $container = new TVBox;
        $container->style = 'width: 100%';
        $container->add(new TXMLBreadCrumb('menu.xml', __CLASS__));
        $container->add($this->form);
        $container->add($panel);

        parent::add( $container );
    }


    public function onClear()
    {
        $fields = $this->form->getFields();
        foreach($fields as $field) {
            TSession::setValue($this->activeRecord.'_filter_'.$field->getName(), NULL);
        }
        TSession::setValue($this->activeRecord.'_filter_data', NULL);
        $this->form->clear();
        $this->onReload();
    }
}
?>