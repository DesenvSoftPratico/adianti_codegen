<?php

ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

class **FORM_CLASS_NAME** extends TPage
{

    private $form;

    public function __construct()
    {

        parent::__construct();

        $this->form = new BootstrapFormBuilder('form_**TABLE_NAME**');
        $this->form->setFormTitle( '**FORM_LABEL**' );
        $this->form->class = 'form_**TABLE_NAME**';

        //Campos do form
**FORM_FIELD_CREATION_LINE**
        //Tamanho da caixa de edição (width) *11.275 (Font Size)
**FIELD_SIZE_BOX**
        //Quantidade de caracteres
**FIELD_SIZE_LINE**
        //Validação
**FIELD_VALIDATION_LINE**
        //Desabilita edição
**FIELD_DESABLE_LINE**
        //Insere Fields no Form
**FORM_FIELD_ADD_LINE**
        $this->form->addFields([new TLabel('')], [TElement::tag('label', '<i><font color="red">Campos obrigatórios</font></i>')]);

        //Botões de ações do form
        $btn = $this->form->addAction( _t('Save'), new TAction(array($this, 'onSave')), 'fa:floppy-o')->class = 'btn btn-sm btn-primary';
        $this->form->addAction( _t('Clear'), new TAction(array($this, 'onEdit')), 'fa:eraser red');
        $this->form->addAction( _t('Back'), new TAction(array('**LIST_NAME**','onReload')), 'fa:arrow-circle-o-left blue');


        //vertical box container //Alair
        $container = new TVBox;
        $container->style = 'width: 100%';
        $container->add(new TXMLBreadCrumb('menu.xml', '**LIST_NAME**'));
        $container->add($this->form);

        parent::add($container);
    }

    function onSave($param = NULL)
    {
        try 
        {
            TTransaction::open('**DB_CONFIG_FILE**');
            $this->form->validate();
            $object = $this->form->getData('**RECORD_NAME**');
            $object->usuarioalteracao = $_SESSION['WebImob']['userid'];
            $object->dataalteracao = date("Y-m-d H:i:s");
            $object->store();
            TTransaction::close();
            $action_ok = new TAction( [ '**LIST_NAME**', "onReload" ] );
            new TMessage( "info", "Registro salvo com sucesso!", $action_ok );
        }
        catch (Exception $e) 
        {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }


    function onEdit($param = NULL)
    {
        try 
        {
            if (isset($param['key'])) 
            {
                $key = $param['key'];
                TTransaction::open('**DB_CONFIG_FILE**');
                $object = new **RECORD_NAME**($key);
                $this->form->setData($object);
                TTransaction::close();
            }
        }
        catch (Exception $e) 
        {
            new TMessage('error', '<b>Error</b> ' . $e->getMessage() . "<br/>");
            TTransaction::rollback();
        }
    }
}
?>